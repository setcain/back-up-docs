Buscar paquete: sudo pacman -Ss package_name
Instalar paquete: sudo pacman -S package_name
Desinstalar paquetes y sus dependencias: sudo pacman -Rs package_name
Update: sudo pacman -Syu
Ver todos los paquetes instalados: sudo pacman -Q

Arch usa systemd como gestor de sistema de arranque:
Listar unidades activas: systemctl
Activar unidad: systemctl start unit_name
Desactivar unidad: systemctl stop unit_name
Ver estado de unidad: systemctl status unit_name
Comprobar estado de unidad: systemctl is-enabled unit_name
Activar inicio atutomatico en el arranque: systemctl enable unit_name
Desactivar inicio automatico en el arranque: systemctl disable unit_name
Manual de una unidad: systemctl help unit_name

Energia:
systemctl reboot, poweroff, suspend, hibernate
