* borrar tablas con drop, pero nunca bases de datos completas.
* las bases de datos completas se borran, eliminando el archivo .db
ejemplo de borrar tabla:
drop table employees;

--- Backup de la db:
redireccionar el output a el path del backup  
.output ./Documents/sqlite3Files/employees.sql 
.dump hacer el backup
.output stdout regresar el output a la terminal

hacer backup con otro tipo de .mode
.mode csv muesta la informacion como comma separated values
.output ./Documents/sqlite3Files/employees.csv configurar el path donde ira el file
select * from employees; hacer el query para que se imprima en employees.csv
.output stdout regresar el output a la terminal

Recuperar el backup:
Abrir de nuevo sqlite3 test.db para recrearla
.read ./Documents/sqlite3Files/employees.sql recupera la db con el path
select * from employees para checar que se haya recuperado la informacion
