Conda (Gestion de paquetes, entornos y dependencias):

En el directorio de anaconda3/envs se guardan los entornos virtuales con sus
configuraciones.

Crear env:
conda create -n name

Eliminar env:
conda remove -n name --all

Activar env en un directorio ya creado:
source activate test

Instalar paquetes:
conda install name

Instalar Django 2.0
conda install -c conda-forge django-configurations

Actualizar paquetes:
conda update name

Ver las instalaciones:
conda list

Informacion de los envs:
conda info -e

Desactivar el env:
source deactivate

Guardar el requirements
pip freeze > requirements.txt
pip install -r requirements.txt
