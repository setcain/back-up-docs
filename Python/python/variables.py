"""
File: variables.py
Author: setcain
Email: setcain@email.com
Github: https://github.com/setcain
Description: Variables y tipos de datos en Python
"""

print('-------------Data types----------------')

string = 'Python'
print(string)
print(type(string))

integer = 3
print(integer)
print(type(integer))

flotante = 1.5
print(flotante)
print(type(flotante))

boolean = True
print(boolean)
print(type(boolean))

none = None
print(none)
print(type(none))

lista = [1, 2, 4, 'hello', True, None]
print(lista)
print(type(lista))

tupla = (1,)
print(tupla)
print(type(tupla))

dictionary = {'name': 'Setcain', 'age': 34}
print(dictionary)
print(type(dictionary))

my_set = set([1, 2, 3, 'mutable'])
print(my_set)
print(type(my_set))

my_frozen_set = frozenset([1, 2, 3, 'inmutable'])
print(my_frozen_set)
print(type(my_frozen_set))

rango = range(9, 15)
print(rango)
print(type(rango))


print('-------------Format----------------')

name = 'set'
# cambiar tipo de dato float(), int(), etc
age = str(34)

# strings format
# concat
print('Hello ' + name + ' ' + age + ' this is concat')
# format method
print('Hello {} {} this is format method'.format(name, age))
# format python 3
print(f'Hello {name} {age} this is f in python 3')


print('-------------Operadores matematicos----------------')

# operador de asignacion
number1 = 1

# operador de preincremento y decremento
# preincremento (y++) y predecremento (y--), no existen en python
number2 = 3
number2 += 1
number2 -= 19

# operador de comparacion
comparacion = (number1 == number2)
