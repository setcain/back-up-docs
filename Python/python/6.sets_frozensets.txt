#-----------------------------------------------------------------------------#
# -------------------------- Conjuntos (sets) ------------------------------- #
#-----------------------------------------------------------------------------#

Identificador: set - {}
Como en matematicas puras (conjuntos) se puede operar con ellos con union,
interseccion y diferencia de conjuntos, son colecciones desordenadas y mutables.

conjunto = set()
conjunto = {'h','o','l','a',}

Agregar solo un elemento: conjunto.add(3)

Agregar varios elementos al conunto: conjunto.update([1,2])

Eliminar un elemento: conjunto.discard(element)

Copiar conjunto: conjunto2 = conjunto.copy()

Limpiar conjunto: conjunto2.clear()

conjunto_2 = {'M','U','N','D','O'}
conjunto_3 = {-2,-1,0,1,2}
conjunto_4 = {-1,0,1,10,20}

Disyuncion: son totalmente distintos?
conjunto_2.isdisjoint(conjunto_3)

Subconjunto: concuerda algun elemento?
conjunto_2.issubset(conjunto_3)

Superconjunto: es contenedor del primer conjunto?
conjunto_2.issuperset(conjunto_3)

unir todos los elemtos:
union = conjunto_2 | conjunto_3 | conjunto_4
metodo_union = conjunto_2.union(conjunto_3)
unir conjunto A con B: con1.update(con2)

imprimir solo los elementos repetidos:
interseccion = conjunto_3 & conjunto_4
metodo_interseccion = conjunto_3.intersection(conjunto_4)

imprimir conjunto de elementos en A pero no en B:
diferencia = conjunto_4 - conjunto_3
metodo_diferencia = conjunto_4.difference(conjunto_3)

imprimir elementos que estan en A o en B pero no en ambos:
diferencia_simetrica = conjunto_3 ^ conjunto_4
metodo_diferencia_simetrica = conjunto_3.symmetric_difference(conjunto_4)

vaciar los elementos del conjunto:
vacio = conjunto & conjunto_2 & conjunto_2 & conjunto_4
set() # conjunto vacio


#-----------------------------------------------------------------------------#
# --------------------- Conjuntos (frozenset) ------------------------------- #
#-----------------------------------------------------------------------------#

Los frozenset tambien son colecciones desordenadas pero a diferencia de los
sets estos son inmutables y son la unica forma de crear conjunto de conjuntos.

my_frozenset = frozenset()
