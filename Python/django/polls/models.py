# polls/models.py
import datetime  # extra import for was_published_recently

from django.db import models
from django.utils import timezone  # extra import for was_published_recently


class Question(models.Model):
    # variables de clase
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('Date published')

    # custom method
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

    def __str__(self):
        return self.question_text


class Choise(models.Model):
    choise_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    # relacion entre Choise y Question
    question = models.ForeignKey(Question, on_delete=models.CASCADE)

    def __str__(self):
        return self.choise_text


# manage.py makemigrations
# Indica que creamos models o les realizamos cambios
# Django guarda estos datos como migraciones en polls/migrations/0001_initial

# manage.py migrate
# Ejecuta las migraciones y gestiona el schema de la db automaticamente

# manage.py check
# revisa que no haya errores antes de iniciar el server

# manage.py sqlmigrate polls 0001
# Para revisar el schema sql que creo Django conforme los modelos creados

# manage.py shell
# Interactuar con la db desde Dajngo shell
