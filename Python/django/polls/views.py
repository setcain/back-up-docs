# polls/views.py
from .models import Question
from django.shortcuts import get_object_or_404, render


def index(request):
    template = 'polls/index.html'

    # regresa las ultimas 5 pregutas ordenadas de la mas antigua a la mas nueva
    latest_question_list = Question.objects.order_by('-pub_date')[:5]

    context = {
        'latest_question_list': latest_question_list,
    }

    return render(request, template, context)


def detail(request, question_id):
    template = 'polls/detail.html'

    question = get_object_or_404(Question, pk=question_id)

    context = {
        'question': question
    }

    return render(request, template, context)


def results(request, question_id):

    response = f"You're looking at the result of question {question_id}"

    return render(response)


def vote(request, question_id):

    return render(f"you're voting on question {question_id}")
