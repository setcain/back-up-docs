Divio/Docker:
divio project up / docker-compose up -d
divio project stop / docker-compose stop

*Setup de Laravel en Divio:
docker-compose run --rm web php /app/divio/setup.php


Lo primero y mas importante en el framework Laravel son las rutas:
routes/web.php

Las vistas estan en:
resources/views

Los controllers estan en:
app/http/controllers
