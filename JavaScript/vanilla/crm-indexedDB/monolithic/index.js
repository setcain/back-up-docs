(function () {
  const listadoClientes = document.getElementById("listado-clientes"); // JS dinamico
  let DB; // Almacena la base de datos de IndexedDB

  window.onload = () => {
    crearDB();

    if (window.indexedDB.open("crm", 1)) {
      obtenerClientes();
    }
  };

  function crearDB() {
    // Crea la base de datos de IndexedDB
    const crearDB = window.indexedDB.open("crm", 1);

    crearDB.onerror = () => console.log("Hubo un error al crear la DB");

    crearDB.onsuccess = () => {
      DB = crearDB.result;
    };

    crearDB.onupgradeneeded = function (e) {
      // Crea columnas en IDBDatabase
      const db = e.target.result; // Contenido IDBDatabase como name, version

      const objectStore = db.createObjectStore("crm", {
        keypath: "id",
        autoIncrement: true,
      });

      objectStore.createIndex("nombre", "nombre", { unique: false });
      objectStore.createIndex("email", "email", { unique: true });
      objectStore.createIndex("telefono", "telefono", { unique: false });
      objectStore.createIndex("empresa", "empresa", { unique: false });
      objectStore.createIndex("id", "id", { unique: true });

      console.log("DB Creada");
    };
  }

  function obtenerClientes() {
    // Imprime la informacion de los registros en el HTML
    const abrirConexion = window.indexedDB.open("crm", 1);

    abrirConexion.onerror = function () {
      console.log("Error al abrir la conexion de indexedDB");
    };

    abrirConexion.onsuccess = function () {
      DB = abrirConexion.result;

      const objectStore = DB.transaction("crm").objectStore("crm");

      // Para leer indexedDB se utiliza cursor
      objectStore.openCursor().onsuccess = function (e) {
        const cursor = e.target.result;

        if (cursor) {
          const { nombre, empresa, email, telefono, id } = cursor.value;

          listadoClientes.innerHTML = `
            <tr>
              <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                <p class="text-sm leading-5 font-medium text-gray-700 text-lg  font-bold"> ${nombre} </p>
                <p class="text-sm leading-10 text-gray-700"> ${email} </p>
              </td>
              <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 ">
                <p class="text-gray-700">${telefono}</p>
              </td>
              <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200  leading-5 text-gray-700">    
                <p class="text-gray-600">${empresa}</p>
              </td>
              <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5">
                <a href="editar-cliente.html?id=${id}" class="text-teal-600 hover:text-teal-900 mr-5">
                  Editar
                </a>
                <a href="#" data-cliente="${id}" class="eliminar text-red-600 hover:text-red-900">
                  Eliminar
                </a>
              </td>
            </tr>
          `;

          cursor.continue();
        } else {
          console.log("No hay mas registros...");
        }
      };
    };
  }
})();
