(function () {
  const formulario = document.getElementById("formulario");
  let DB;

  window.onload = () => {
    conectarDB();

    formulario.addEventListener("submit", validarCliente);
  };

  function conectarDB() {
    const abrirConexion = window.indexedDB.open("crm", 1);

    abrirConexion.onerror = function () {
      console.log("Hay un error al conectar IndexedDB");
    };

    abrirConexion.onsuccess = function () {
      DB = abrirConexion.result;
    };
  }

  function validarCliente(e) {
    // Valida los datos del formulario
    e.preventDefault();

    const nombre = document.getElementById("nombre").value;
    const email = document.getElementById("email").value;
    const telefono = document.getElementById("telefono").value;
    const empresa = document.getElementById("empresa").value;

    if (nombre === "" || email === "" || telefono === "" || empresa === "") {
      imprimirAlerta("Todos los campos son obligatorios", "error");

      return;
    }

    // Crear objeto con datos de nuevo cliente del formulario
    // object literal enhancement, lo contrario del destructuring de objs
    const cliente = {
      nombre,
      email,
      telefono,
      empresa,
      id: Date.now(),
    };

    crearNuevoClente(cliente);
  }

  function crearNuevoClente(cliente) {
    // Crear nuevo cliente en IndexedDB
    const transaction = DB.transaction(["crm"], "readwrite");

    const objectStore = transaction.objectStore("crm");

    objectStore.add(cliente);

    transaction.onerror = function () {
      imprimirAlerta("Error al agregar nuevo cliente a IndexedDB");
    };

    transaction.oncomplete = function () {
      imprimirAlerta("Cliente agregado correctamente");

      setTimeout(() => {
        // Redirecciona a la pagina principal
        window.location.href = "index.html";
      }, 2500);
    };
  }

  function imprimirAlerta(msj, tipo) {
    // Crea alerta dinamicamente con JS en HTML
    const alerta = document.querySelector(".alerta");
    const div = document.createElement("div");

    if (!alerta) {
      div.classList(
        "px-4",
        "py-3",
        "rounded",
        "max-w-lg",
        "mx-auto",
        "mt-6",
        "text-center",
        "border",
        "alerta"
      );

      if (tipo === "error") {
        div.classList.add("bg-red-100", "border-red-400", "text-red-700");
      } else {
        div.classList.add("bg-green-100", "border-green-400", "text-green-700");
      }

      div.textContent = msj;

      formulario.appendChild(div);

      setTimeout(() => {
        div.remove();
      }, 2500);
    }
  }
})();
