(function () {
  const nombreInput = document.getElementById("nombre");
  const emailInput = document.getElementById("email");
  const telefonoInput = document.getElementById("telefono");
  const empresaInput = document.getElementById("empresa");
  const formulario = document.getElementById("formulario");
  let DB;
  let idCliente;

  window.onload = () => {
    conectarDB();

    // Actualiza el registro con los datos editados
    formulario.addEventListener("submit", actualizarCliente);

    // Verifica el id de la url
    const parametrosURL = new URLSearchParams(window.location.search);
    idCliente = parametrosURL.get("id");

    if (idCliente) {
      setTimeout(() => {
        // asincronismo antes de ver async/await
        obtenerCliente(idCliente);
      }, 500);
    }
  };

  function conectarDB() {
    const abrirConexion = window.indexedDB.open("crm", 1);

    abrirConexion.onerror = function () {
      console.log("Hay un error al conectar IndexedDB");
    };

    abrirConexion.onsuccess = function () {
      DB = abrirConexion.result;
    };
  }

  function actualizarCliente(e) {
    e.preventDefault();

    if (
      // Validacion
      nombreInput.value === "" ||
      emailInput === "" ||
      telefonoInput === "" ||
      empresaInput === ""
    ) {
      imprimirAlerta("Todos los campos son obligatorios", "error");

      return;
    }

    // Actualizar cliente y mandar a pagina principal
    const clienteActualizado = {
      nombre: nombreInput.value,
      email: emailInput.value,
      empresa: empresaInput.value,
      telefono: telefonoInput.value,
      id: parseFloat(idCliente),
    };

    const transaction = DB.transaction(["crm"], "readwrite");
    const objectStore = transaction.objectStore("crm");

    objectStore.put(clienteActualizado);

    transaction.oncomplete = function () {
      imprimirAlerta("Editado correctamente");

      setTimeout(() => {
        window.location.href = "index.html";
      }, 2500);
    };

    transaction.onerror = function (error) {
      // TODO:  <23-12-20, ricardoveronica>
      // Aunque se actualice bien manda error //
      console.log(error.target.error);
      imprimirAlerta("Error al actualizar el registro", "error");
    };
  }

  function obtenerCliente(idCliente) {
    const transaction = DB.transaction(["crm"], "readonly");
    const objectStore = transaction.objectStore("crm");
    const cliente = objectStore.openCursor();

    cliente.onsuccess = function (e) {
      const cursor = e.target.result;

      if (cursor) {
        if (cursor.value.id === Number(idCliente)) {
          llenarFormulario(cursor.value);
        }

        cursor.continue();
      }
    };
  }

  function llenarFormulario(datosCliente) {
    const { nombre, email, telefono, empresa } = datosCliente;
    nombreInput.value = nombre;
    emailInput.value = email;
    telefonoInput.value = telefono;
    empresaInput.value = empresa;
  }

  function imprimirAlerta(mensaje, tipo) {
    // Crea alerta dinamicamente con javascript en el HTML
    const alerta = document.querySelector(".alerta");
    const div = document.createElement("div");

    if (!alerta) {
      div.classList.add(
        "px-4",
        "py-3",
        "rounded",
        "max-w-lg",
        "mx-auto",
        "mt-6",
        "text-center",
        "border",
        "alerta"
      );

      if (tipo === "error") {
        div.classList.add("bg-red-100", "border-red-400", "text-red-700");
      } else {
        div.classList.add("bg-green-100", "border-green-400", "text-green-700");
      }

      div.textContent = mensaje;

      formulario.appendChild(div);

      setTimeout(() => {
        div.remove();
      }, 2500);
    }
  }
})();
