function crearDB() {
  // Crea IndexedDb
  const crearDB = window.indexedDB.open("crm", 1); // Crea nombre y version DB

  crearDB.onerror = function () {
    // Si no se crea IndexedDb con nombre y version
    console.log(false);
  };

  crearDB.onsucces = function () {
    // Si se crea, abre la conexion a la DB
    crearDB.result;
  };

  crearDB.onupgradeneeded = function (e) {
    const db = e.target.result; // IDBDatabase obj

    const objectStore = db.createObjectStore("crm", {
      // Declara cual sera la llave primaria
      keyPath: "id",
      autoIncrement: true,
    });

    // Crea columnas
    objectStore.createIndex("id", "id", { unique: true });
    objectStore.createIndex("nombre", "nombre", { unique: false });
    objectStore.createIndex("correo", "correo", { unique: true });
    objectStore.createIndex("telefono", "telefono", { unique: false });
    objectStore.createIndex("empresa", "empresa", { unique: false });
  };
}

export default crearDB;
