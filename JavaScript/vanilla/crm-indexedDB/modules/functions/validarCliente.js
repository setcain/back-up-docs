import imprimirAlerta from "./imprimirAlerta";

function validarCliente(e) {
  // Valida los datos del formulario
  e.preventDefault();

  // Lee los inputs del formluario
  const nombre = document.getElementById("nombre").value;
  const email = document.getElementById("email").value;
  const telefono = document.getElementById("telefono").value;
  const empresa = document.getElementById("empresa").value;

  if (nombre === "" || email === "" || telefono === "" || empresa === "") {
    imprimirAlerta("Todos los campos son obligatorios", "error");
  }
}

export default validarCliente;
