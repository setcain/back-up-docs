function imprimirAlerta(mensaje, tipoMensaje) {
  // Imprime las alertas correspondientes en el HTML
  const formulario = document.getElementById("formulario");
  const alerta = document.querySelector(".alerta");

  if (!alerta) {
    const div = document.createElement("div");
    div.textContent = mensaje;
    div.classList.add(
      "px-4",
      "py-3",
      "rounded",
      "max-w-lg",
      "mx-auto",
      "mt-6",
      "text-center",
      "border",
      "alerta"
    );

    tipoMensaje === "error"
      ? div.classList.add("bg-red-100", "border-red-400", "text-red-700")
      : div.classList.add("bg-green-100", "border-green-400", "text-green-700");

    formulario.appendChild(div);

    setTimeout(() => {
      div.remove();
    }, 2500);
  }
}

export default imprimirAlerta;
