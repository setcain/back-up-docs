import validarCliente from "./functions/validarCliente";
import crearDB from "./functions/crearDB";
import conectarDB from "./functions/conectarDB";

const formulario = document.getElementById("formulario");

window.onload = () => {
  formulario.addEventListener("submit", validarCliente);

  crearDB();
  conectarDB();
};
