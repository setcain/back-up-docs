import mostrarError from "./mostrarError";
import consultarAPI from "./consultarAPI";

function validarFormulario() {
  // Valida datos que introduce el usuario en el formulario
  const ciudad = document.getElementById("ciudad").value;
  const pais = document.getElementById("pais").value;

  if (ciudad === "" || pais === "") {
    mostrarError("Todos los campos son obligatorios");
  }

  consultarAPI(cuidad, pais);
}

export default validarFormulario;
