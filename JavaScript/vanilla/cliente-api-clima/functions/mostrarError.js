function mostrarError(mensaje) {
  // Muestra alerta de error en el HTML
  const alerta = document.querySelector(".bg-red-100");

  if (!alerta) {
    // Crea div para mostrar el mensaje de error
    const container = document.querySelector(".container");
    const div = document.createElement("div");

    // Agrega clases al div creado
    div.classList.add(
      "bg-red-100",
      "border-red-400",
      "text-red-700",
      "px-4",
      "py-3",
      "rounded",
      "max-w-md",
      "mx-auto",
      "mt-6",
      "text-center"
    );

    // Agrega mensaje al div de error
    div.innerHTML = `
      <strong class="font-bold">Error!</strong>
      <span class="block">${mensaje}</span>
    `;

    // Agrega el div de error al HTML
    container.appendChild(div);

    // Elimina el div con el mensaje despues de 2.5 segs.
    setTimeout(() => {
      div.remove();
    }, 2500);
  }
}

export default mostrarError;
