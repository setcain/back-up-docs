import mostrarImagenes from "./mostrarImagenes";
import calcularPaginas from "../helpers/calcularPaginas";

function mostrarImagens(imagenes) {
  // Busca imagenes en la API de Pixabay
  const key = "xxx";
  const url = `https://pixabay.com/api/?key=${key}&q=${imagenes}&per_page=100`;

  fetch(url)
    .then((resultado) => resultado.json())
    .then((resultado) => {
      const totalPaginas = calcularPaginas(resultado.totalHits);
      mostrarImagenes(resultado.hits);
    });
}

export default mostrarImagens;
