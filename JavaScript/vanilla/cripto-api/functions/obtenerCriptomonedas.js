// Obtine las criptomonedas de la API por medio de una promise
const obtenerCriptomonedas = (criptomonedas) =>
  new Promise((resolve) => {
    resolve(criptomonedas);
  });

export default obtenerCriptomonedas;
