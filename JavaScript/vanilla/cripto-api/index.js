/*
 * Consumir API de criptomonedas con JS
 * */
import consultarCriptomonedas from "../functions/consultarCriptomonedas";

window.onload = () => {
  consultarCriptomonedas();
};
