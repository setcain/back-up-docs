class Cita {
  // Crea arreglo vacio de citas
  constructor() {
    this.citas = [];
  }

  agregarCita(cita) {
    // Agrega nueva cita el arreglo citas, por medio de citaObj
    this.citas = [...this.citas, cita];
  }

  eliminarCita(id) {
    // Eliminar cita por id
    this.citas = this.citas.filter((cita) => {
      cita.id !== id;
    });
  }

  editarCita(citaActualizada) {
    // Edita la cita
    this.citas = this.citas.map((cita) =>
      cita.id === citaActualizada.id ? citaActualizada : cita
    );
  }
}

export default Cita;
