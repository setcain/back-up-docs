// Crear project/index.js para las configuraciones
// En Node se usa commonjs, en versiones mas recientes soporta ES6+ modules
const express = require("express");
const bodyParser = require("body-parser"); // leer lo que tiene los req
const path = require("path"); // Lee el fs del proyecto
const port = process.env.PORT || 3000; // Configura el puerto || .env

const routes = require("./routes"); // Importa Router
const db = require("./conf/db"); // Trae las configuraciones de la base de datos

const app = express(); // Inicia la app con express

// Conecta la base de datos
require("./models/Project"); // Crea el modelo
db.sync() // .authenticate
  .then(() => console.log("Conectado al servidor de la base de datos"))
  .catch((err) => console.log(err));

app.set("view engine", "pug"); // Importa las vistas
app.set("views", path.join(__dirname, "./views")); // Donde van las vistas
app.use(express.static("public")); // Donde van los staticos
app.use("/", routes()); // Importa Router

app.listen(port, "0.0.0.0", () => {
  console.log(`Express server on: http://0.0.0.0:${port}`);
});

//-------------------- Router --------------------//
// Es el encargado de registrar las url o endpoints del proyecto
// En routes/index.js
// Express router
const express = require("express");
const router = express.Router();
// Importa controlador
const projectsController = require("../controllers/projectsController");

module.exports = function () {
  router.get("/", projectsController.projectsHome); // Usa Controller

  return router;
};

//-------------------- Controlador --------------------//
// El que conecta el modelo con la vista
// En controllers/projectsController.js
exports.projectsHome = (req, res) => {
  res.render("index", {
    // Renderiza data en index.pug de las Vistas
    variable: "Valor", // Manda variable a la vista index.pug
  });
};

//-------------------- Vistas --------------------//
// El que mustra los resultados del modelo en html
// En views/index.pug
// npm install pug // Instalar el template engine con npm

h1 = variable; // Imprime valor de la variable del Controlador

// Estaticos en layout.pug
link((rel = "stylesheet"), (href = "/css/style.css")); // /public/css/style.css

//-------------------- Modelos --------------------//
// ORM sequielize y mysql en docker para la base de datos
// npm install mysql2 sequealize

// En config/db.js
const { Sequelize } = require("sequelize");

const Sequelize = new Sequelize("db_name", "user_name", "password", {
  host: "db", //docker
  dialect: "mysql",
  port: "3306",
  operatorsAliases: false,
  define: {
    timestamps: false,
  },
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
});

module.exports = Sequelize;

// En models/Projects.js
const Sequelize = require("sequelize");
const db = require("../config/db");

const Projects = db.define("projects", {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  name: Sequelize.STRING,
  url: Sequelize.STRING,
});

module.exports = Projects;
