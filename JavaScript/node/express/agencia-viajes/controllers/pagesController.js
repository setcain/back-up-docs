// Controller encargado de pasar los datos a las vistas

/*
 * Existen mas metodos de respuesta para enviar datos como:
 * res.send() // Imprime texto plano
 * res.render() // Para renderizar una vista html con template engines
 * res.json({id: 1}) // Para APIs
 * */

// Variables de contexto:
// Ejemplo pasar variables, como en el diccionario de contexto de django
// router.get("/", (req, res) => {
// const title = "Este es el tittulo de la pagina"; // Variable
// res.render("about", { title: title }); // Diccionario de contexto
// res.render("about", { title }); // Object literal enhancement
// });
const initPage = (req, res) => {
  // render busca init.pug en views
  res.render("init", {
    // variables que pasan al contexto de pug
    pag: "inicio",
  });
};

const aboutPage = (req, res) => {
  res.render("about", {
    pag: "nosotros",
  });
};

const viajesPage = (req, res) => {
  res.render("viajes", {
    pag: "viajes",
  });
};

const testimonialesPage = (req, res) => {
  res.render("testimoniales", {
    pag: "testimoniales",
  });
};

export { initPage, aboutPage, viajesPage, testimonialesPage };
