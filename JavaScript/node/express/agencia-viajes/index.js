/*
 * Estas son las configuraciones de Express.
 * Las rutas, las vistas, los modelos deben de estar en otro directorio
 * para hacer todo modular.
 * */
import express from "express"; // Core express
import router from "./routes/index.js"; // Router
import db from "./config/db.js"; // Database

// Iniciar express
const app = express();

// Conecta base de datos
db.authenticate()
  .then(() => console.log("Database connected"))
  .catch((error) => console.log(error));

// Variables
const port = process.env.PORT || 3000;

// Template engine pug
app.set("view engine", "pug");

// Middlewares
// Define la carpeta de archivos estaticos
app.use(express.static("public"));

// Ejemplo Middleware
app.use((req, res, next) => {
  // res.locals son variables locales de Express
  const year = new Date();
  res.locals.yearVar = year.getFullYear();
  res.locals.siteName = "Agencia de Viajes";

  return next(); // obliga a pasar al siguiente middleware terminando este
});

// Router
app.use("/", router);

// Inicia el server
app.listen(port, "0.0.0.0", () => {
  console.log(`Express server on: http://0.0.0.0:${port}`);
});
