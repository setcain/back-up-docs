import Sequelize from "sequelize";
import db from "../config/db.js";

// Tabla con campos y valor del campo
export const Viaje = db.define("viaje", {
  title: { type: Sequelize.STRING },
  price: { type: Sequelize.STRING },
  date_out: { type: Sequelize.DATE },
  date_back: { type: Sequelize.DATE },
  image: { type: Sequelize.STRING },
  description: { type: Sequelize.STRING },
  disponibility: { type: Sequelize.STRING },
  slug: { type: Sequelize.STRING },
});
