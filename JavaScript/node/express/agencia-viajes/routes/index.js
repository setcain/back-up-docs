/*
 * Se pueden utilizar todos los metodos http como delete, patch, put, post, get
 * pero exite el .use el cual generaliza todos los metodos
 * */
import express from "express"; // Core de express
import {
  initPage,
  aboutPage,
  viajesPage,
  testimonialesPage,
} from "../controllers/pagesController.js"; // Controladores

const router = express.Router(); // Router de express

// url y controlador
router.get("/", initPage);

// url y controlador
router.get("/about", aboutPage);

// url y controlador
router.get("/viajes", viajesPage);

// url y controlador
router.get("/testimoniales", testimonialesPage);

export default router;
