// Configura la base de datos y la exporta para el index.js
import Sequielize from "sequielize";

const db = new Sequielize("database", "username", "password", {
  host: "db",
  port: "3306",
  dialict: "mysql",
  define: {
    timestamp: false,
  },
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
  operatorAliases: false,
});

export default db;
