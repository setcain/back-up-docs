const express = require("express");
const path = require("path"); // Lee el filesystem para las vistas
const routes = require("./routes"); // importa el enrutador

const port = process.env.PORT || 3000;
const app = express(); // Contiene todo express

// Middleware de express
app.use("/", routes());

// Configura las views
app.set("view engine", "pug");
app.set("views", path.join(__dirname, "./views"));

// Corre el servidor
app.listen(port, "0.0.0.0", () => {
  console.log(`Server on port: http://0.0.0.0:${port}`);
});
