const express = require("express");
const router = express.Router();
const proyectosController = require("../controllers/index");

// Rutas para la aplicacion
module.exports = function () {
  router.get("/", proyectosController.proyectosHome);

  return router;
};
