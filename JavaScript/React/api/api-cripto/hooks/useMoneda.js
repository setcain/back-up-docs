import React, { useState } from "react";
import styled from "@emotion/styled";
import PropTypes from "prop-types";

const Label = styled.label`
  display: block;
  color: #fff;
  font-family: "Bebas Neue", cursive;
  font-size: 2.4rem;
`;

const Select = styled.select`
  display: block;
  border: none;
  border-radius: 10px;
  padding: 1rem;
  width: 100%;
  font-size: 1.1rem;
`;

const useMoneda = (label, initialState, monedas) => {
  const [stateMoneda, setState] = useState(initialState);

  const SeleccionaMoneda = () => {
    <>
      <Label>{label}</Label>
      <Select onChange={(e) => setState(e.target.value)} value={stateMoneda}>
        <option value="">MONEDA</option>
        {monedas.map((moneda) => (
          <option key={moneda.codigo} value={moneda.codigo}>
            {moneda.nombre}
          </option>
        ))}
      </Select>
    </>;
  };

  return [stateMoneda, SeleccionaMoneda, setState];
};

useMoneda.propTypes = {
  label: PropTypes.string,
  initialState: PropTypes.string,
  monedas: PropTypes.array,
};

export default useMoneda;
