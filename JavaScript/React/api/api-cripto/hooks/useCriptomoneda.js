import React, { useState } from "react";
import styled from "@emotion/styled";
import PropTypes from "prop-types";

const Label = styled.label`
  display: block;
  color: #fff;
  font-family: "Bebas Neue", cursive;
  font-size: 2.4rem;
`;

const Select = styled.select`
  display: block;
  border: none;
  border-radius: 10px;
  padding: 1rem;
  width: 100%;
  font-size: 1.1rem;
`;

const useCriptomoneda = (label, initialState, listaCriptomonedas) => {
  const [stateCripto, setState] = useState(initialState);

  const SeleccionaCripto = () => {
    <>
      <Label>{label}</Label>
      <Select onChange={(e) => setState(e.target.value)} value={stateMoneda}>
        <option value="">CRIPTOMONEDA</option>
        {listaCriptomonedas.map((item) => (
          <option key={item.CoinInfo.Id} value={item.CoinInfo.Name}>
            {item.CoinInfo.Fullname}
          </option>
        ))}
      </Select>
    </>;
  };

  return [stateCripto, SeleccionaCripto, setState];
};

useCriptomoneda.propTypes = {
  label: PropTypes.string,
  initialState: PropTypes.string,
  listaCriptomonedas: PropTypes.array,
};

export default useCriptomoneda;
