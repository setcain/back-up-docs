import React, { useState, useEffect } from "react";
import axios from "axios";
import styled from "@emotion/styled";
import useMoneda from "../hooks/useMoneda";
import useCriptomoneda from "../hooks/useCriptomoneda";
import Error from "./Error";
import PropTypes from "prop-types";

const Button = styled.button`
  margin-top: 20px;
  font-weight: bold;
  font-size: 20px;
  padding: 10px;
  background-color: #66a2fe;
  border: none;
  width: 100%;
  border-radius: 10px;
  color: #fff;
  transition: background-color, 0.3s, ease;
  &:hover {
    background-color: #326ac0;
    cursor: pointer;
  }
`;

function Form({ setMoneda, setCriptomoneda }) {
  // states
  const [listaCriptomonedas, setListacriptomonedas] = useState([]);
  const [error, setError] = useState(false);

  // Variables
  const MONEDAS = [
    { codigo: "USD", nombre: "Dolar de Estados Unidos" },
    { codigo: "MXN", nombre: "Peso Mexicano" },
    { codigo: "EUR", nombre: "Euro" },
    { codigo: "GBP", nombre: "Libra Esterlina" },
  ];

  // custom hooks
  const [stateMoneda, SeleccionaMoneda] = useMoneda(
    "Elige tu moneda",
    "",
    MONEDAS
  );

  const [stateCripto, SeleccionaCripto] = useCriptomoneda(
    "Elige tu criptomoneda",
    "",
    listaCriptomonedas
  );

  // effects
  useEffect(() => {
    const consultarAPI = async () => {
      const url = `http://...`;

      const resultado = await axios.get(url);

      setListacriptomonedas(resultado.data.Data);
    };
    consultarAPI();
  }, []);

  // events
  function cotizarMoneda(e) {
    e.preventDefault();

    if (stateMoneda === "" || stateCripto === "") {
      setError(true);
      return;
    }

    setError(false);

    setMoneda(stateMoneda);

    setCriptomoneda(stateCripto);
  }

  return (
    <form>
      {error && <Error mensaje="Todos los campos son obligatorios" />}

      <SeleccionaMoneda />

      <SeleccionaCripto />

      <Button type="submit" onClick={cotizarMoneda}>
        Calcular
      </Button>
    </form>
  );
}

Form.propTypes = {
  setMoneda: PropTypes.func,
  setCriptomoneda: PropTypes.func,
};

export default Form;
