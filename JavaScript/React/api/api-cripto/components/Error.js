import sytled from "@emotion/styled";
import PropTypes from "prop-types";

const MensajeError = styled.p`
  padding: 1rem;
  color: #fff;
  font-family: "Bebas Neue", cursive;
  font-size: 30px;
  text-align: center;
  text-transform: uppercase;
  background-color: #b7322c;
`;

function Error({ mensaje }) {
  return <MensajeError>{mensaje}</MensajeError>;
}

Error.propTypes = {
  mensaje: PropTypes.string,
};

export default Error;
