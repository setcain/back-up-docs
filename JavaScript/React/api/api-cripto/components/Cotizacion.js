import styled from "@emotion/styled";
import PropTypes from "prop-types";

const Div = styled.div`
  color: #fff;
  font-family: Arial, Helvetica, sans-serif;
  span {
    font-weight: bold;
  }
`;

const Precio = styled.p`
  font-size: 30px;
`;

const P = styled.p`
  font-size: 18px;
`;

function Cotizacion({ resultado }) {
  if (Object.keys(resultado).length === 0) return null;

  return (
    <Div>
      <Precio>
        El precio es: <span>{resultado.PRICE}</span>
      </Precio>
      <P>
        Precio mas alto del dia: <span>{resultado.HIGHDAY}</span>
      </P>
      <P>
        Precio mas bajo del dia: <span>{resultado.LOWDAY}</span>
      </P>
      <P>
        Variacion ultimas 24HRS: <span>{resultado.CHANGEPCT24HOUR}</span>
      </P>
      <P>
        Ultima actualizacion: <span>{resultado.LASTUPDATE}</span>
      </P>
    </Div>
  );
}

Cotizacion.propTypes = {
  resultado: PropTypes.object,
};

export default Cotizacion;
