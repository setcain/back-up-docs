import React, { useState, useEffect } from "react";
import styled from "@emotion/styled";
import axios from "axios";
import imagen from "./cryptomonedas.png";
import Form from "./components/Form";
import Cotizacion from "./components/Cotizacion";
import Spinner from "./components/spinner/Spinner";

const Container = styled.div`
  max-width: 900px;
  margin: 0 auto;

  @media (min-width) {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    column-gap: 2rem;
  }
`;

const Imagen = styled.img`
  max-width: 100px;
  margin-top: 5rem;
`;

const Heading = styled.h1`
  font-family: "Bebas Neue", cursive;
  color: #fff;
  text-align: left;
  font-weight: 700px;
  font-size: 50px;
  margin-bottom: 50px;
  margin-top: 80px;

  &::after {
    display: block;
    content: "";
    width: 100px;
    height: 5px;
    background-color: #66a2fe;
  }
`;

function App() {
  const [moneda, setMoneda] = useState("");
  const [criptomoneda, setCriptomoneda] = useState("");
  const [resultado, setResultado] = useState({});
  const [spinner, setSpinner] = useState(false);

  useEffect(() => {
    (async function cotizarCriptomoneda() {
      if (moneda === "") return null;

      const url = `https://...`;

      const result = await axios.get(url);

      setSpinner(true);

      setTimeout(() => {
        setSpinner(false);
        setResultado(result.data.DISPLAY[criptomoneda][moneda]);
      }, 2500);
    })();
  }, [moneda, criptomoneda]);

  const component = spinner ? (
    <Spinner />
  ) : (
    <Cotizacion resultado={resultado} />
  );

  return (
    <Container>
      <div>
        <Imagen src={imagen} alt="Imagen crypto" />
      </div>
      <div>
        <Heading>Cotiza criptomonedas al instante</Heading>
        <Form setMoneda={setMoneda} setCriptomoneda={setCriptomoneda} />
        {component}
      </div>
    </Container>
  );
}

export default App;
