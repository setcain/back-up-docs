import React, { useState, useEffect } from "react";
import Header from "./components/Header";
import Form from "./components/Form";
import Clima from "./components/Clima";
import Error from "./components/Error";

function App() {
  const [consultar, setConsultar] = useState(false);
  const [error, setError] = useState(false)
  const [resultado, setResultado] = useState({})
  const [busqueda, setBusqueda] = useState({
    ciudad: "",
    pais: "",
  });
  const { ciudad, pais } = busqueda;

  useEffect(() => {
    (function consultarApi() {
      if (consultar) {
        const apiKey = "6b31c457162df5d14e1155fcfb8dbafa";
        const api = `http://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=${apiKey}`;

        const respuesta = await fetch(api)
        const data = await respuesta.json()

        setResultado(data)
        
        setConsultar(false)

        if (resultado.cod === '404') {
          setError(true) 
        } else {
          setError(false)
        }
      }
    })();
  }, [consultar, pais, consultar, resultado]);

  let component
  if (error) component = <Error mensaje="No hay resultados"/>
  if (!error) component = <Clima resultado={resultado} />

  return (
    <>
      <Header title={"Clima React App"} />

      <div className="contenedor-form">
        <div className="container">
          <div className="row">
            <div className="col m6 s12">
              <Form
                busqueda={busqueda}
                setBusqueda={setBusqueda}
                setConsultar={setConsultar}
              />
            </div>
            <div className="col m6 s12">
              {component}
            </div>
          </div>
        </div>
      </div>
      <Form />
    </>
  );
}

export default App;
