function Clima({ resultado }) {
  const { name, main, weather } = resultado;
  const kelvin = 273.15;

  if (!name) return null;

  return (
    <div className="card-panel white col s12">
      <div className="black-text">
        <h2>El clima de {name} es: </h2>
        <p className="temperatura">
          {parseInt(main.temp - kelvin, 10).toFixed(0)}
          <span>&#x2103</span>
        </p>
        <p>
          Sky: <i>{weather[0].main}</i>
        </p>
        <p>
          Max: {parseInt(main.temp_max - kelvin, 10).toFixed(0)}
          <span>&#x2103</span>
        </p>
        <p>
          Min: {parseInt(main.temp_min - kelvin, 10).toFixed(0)}
          <span>&#x2103</span>
        </p>
      </div>
    </div>
  );
}

export default Clima;
