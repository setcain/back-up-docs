import React, { useState } from "react";
import Error from "./Error";

function Form({ busqueda, setBusqueda, setConsultar }) {
  const [err, setErr] = useState(false);
  const { ciudad, pais } = busqueda;

  const handleChange = (e) => {
    setBusqueda({
      ...busqueda,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (ciudad.trim() === "" || pais.trim() === "") {
      setErr(true);
    }

    // Quitar el error cuando los campos son llenados correctamente
    setErr(false);

    // Consultar la api
    setConsultar(true);
  };

  return (
    <form onSubmit={handleSubmit}>
      {err && <Error mensaje="Ambos campos son obligatorios" />}

      <div className="input-field col s12">
        <input
          type="text"
          name="ciudad"
          id="ciudad"
          value={ciudad}
          onChange={handleChange}
        />
        <label htmlFor="ciudad">Ciudad: </label>
      </div>

      <div className="input-field col s12">
        <select id="pais" name="pais" value={pais} onChange={handleChange}>
          <option value="">-- Selecciona un Pais --</option>
          <option value="MX">Mexico</option>
          <option value="US">United States</option>
          <option value="AR">Argentina</option>
          <option value="CO">Colombia</option>
          <option value="CR">Costa Rica</option>
          <option value="ES">Spain</option>
          <option value="PE">Peru</option>
        </select>
      </div>

      <div className="input-field col s12">
        <button
          type="submit"
          className="waves-effect waves-light btn-large btn-block yellow accent-4"
        >
          Bucar Clima
        </button>
      </div>
    </form>
  );
}

export default Form;
