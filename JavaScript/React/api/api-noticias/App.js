import Header from "./components/Header";
import Form from "./components/Form";

function App() {
  return (
    <>
      <Header title="Noticiero Web" />
      <div>
        <Form title="encuentra noticias por categoria" />
      </div>
    </>
  );
}

export default App;
