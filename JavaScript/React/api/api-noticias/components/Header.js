// import React, {useState} from "react"

function Header({ title }) {
  return (
    <nav>
      <a>{title}</a>
    </nav>
  );
}

export default Header;
