import React, { useState, useEffect } from "react";
import Frases from "./components/Frases";
import styled from "@emotion/styled";

const Contenedor = styled.div`
  display: flex;
  align-items: center;
  padding-top: 5rem;
  flex-direction: column;
`;

const Boton = styled.button`
  background: --webkit-linear-gradient(
    top left,
    #007d35 0%,
    #007d35 40%,
    #0f574e 100%
  );
  background-size: 300px;
  border: 2px solid black;
  margin-top: 3rem;
  padding: 1rem 3rem;
  color: #fff;
  font-family: Arial, Arial, Helvetica, sans-serif;
  font-size: 2rem;
  transition: background-size 1s ease;

  :hover {
    cursor: pointer;
    background-size: 50px;
  }
`;

function App() {
  // Estado que guarda un objeto vacio
  const [frase, setFrase] = useState({});

  async function consultarApi() {
    // Fetch con Promises
    // const api = fetch("https://breaking-bad-quotes.herokuapp.com/v1/");
    // const frase = api.then((respuesta) => respuesta.json());
    // frase.then(setFrase(frase[0]));

    // Fetch con async/await para traer los datos de la api
    const url = await fetch(
      "https://breaking-bad-quotes.herokuapp.com/v1/quotes"
    );

    const frase = await url.json();

    setFrase(frase[0]);
  }

  useEffect(() => {
    // Solo se ejecuta una vez con el render
    consultarApi();
  }, []);

  return (
    <Contenedor onClick={consultarApi}>
      <Frases frase={frase} />
      <Boton>Obtener Frase</Boton>
    </Contenedor>
  );
}

export default App;
