import styled from "@emotion/styled";

const ContenedorFrase = styled.div`
  max-width: 800px;
  background-color: #fff;
  border-radius: 0.5rem;
  padding: 3rem;

  @media (min-width: 992px) {
    margin-top: 10rem;
  }

  h2 {
    position: relative;
    padding-left: 4rem;
    font-family: Arial, Helvetica, sans-serif;
    text-align: center;
  }

  p {
    font-family: Verdana, Geneva, Tahoma, sans-serif;
    font-size: 1.4rem;
    font-weight: bold;
    text-align: right;
    color: #666;
    margin-top: 2rem;
  }
`;

function Frases({ frase }) {
  // Si no hay frase regresa null para no mostrar nada en el componente
  if (Object.keys(frase).length === 0) return null;

  return (
    <ContenedorFrase>
      <h2>{frase.quote}</h2>
      <p>{frase.author}</p>
    </ContenedorFrase>
  );
}

export default Frases;
