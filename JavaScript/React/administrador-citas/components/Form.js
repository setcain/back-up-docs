import React, { useState } from "react";
import { v4 as uuid4 } from "uuid";
import PropTypes from "prop-types";

function Form({ crearCita }) {
  const [cita, setCita] = useState({
    mascota: "",
    propietario: "",
    fecha: "",
    hora: "",
    sintomas: "",
  });
  const [error, setError] = useState(false);
  const { mascota, propietario, fecha, hora, sintomas } = cita;

  const handleClick = (e) => {
    e.preventDefault();
    if (
      mascota.trim() === "" ||
      propietario.trim() === "" ||
      fecha === "" ||
      hora === "" ||
      sintomas.trim()
    ) {
      setError(true);
      return;
    }

    setError(true);

    cita.id = uuid4();

    crearCita();

    setCita({
      mascota: "",
      propietario: "",
      fecha: "",
      hora: "",
      sintomas: "",
    });
  };

  const handleInput = (e) => {
    setCita({
      ...cita,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <>
      <h2>Crear Cita</h2>

      {error ? (
        <p className="alerta-error">Todos los campos son obligatorios</p>
      ) : null}

      <form>
        <label>Nombre mascota</label>
        <input
          type="text"
          name="mascota"
          className="u-full-width"
          placehol="Nombre de la mascota"
          onInput={handleInput}
        />

        <label>Nombre del propietario</label>
        <input
          type="text"
          name="propietario"
          className="u-full-width"
          placehol="Nombre del propietario"
          onInput={handleInput}
        />

        <label>Fecha</label>
        <input
          type="date"
          name="fecha"
          className="u-full-width"
          onInput={handleInput}
        />

        <label>Hora</label>
        <input
          type="time"
          name="hora"
          className="u-full-width"
          onInput={handleInput}
        />

        <label>Sintomas</label>
        <textarea
          name="sintomas"
          className="u-full-width"
          onInput={handleInput}
        ></textarea>

        <button
          type="submit"
          className="u-full-widht button-primary"
          onClick={handleClick}
        >
          Agregar cita
        </button>
      </form>
    </>
  );
}

Formulario.propTypes = {
  crearCita: PropTypes.func.isRequired,
};

export default Form;
