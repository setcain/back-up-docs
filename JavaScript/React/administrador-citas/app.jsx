im;
import Form from "./components/Form";
import Cita from "./components/Cita";

function App() {
  return (
    <>
      <div>Administrador de pacientes</div>

      {/* Skeleton CSS */}
      <div className="container">
        <div className="row">
          <div className="one-half column">
            <Form />
          </div>
          <div className="one-half column">
            <Component />
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
