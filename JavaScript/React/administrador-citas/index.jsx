import React from "react";
import ReactDOM from "react-dom";
import App from "./app";

ReactDOM.render(
  <React.StricMode>
    <App />
  </React.StricMode>,
  document.getElementById("root")
);
