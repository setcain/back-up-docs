import React, { useState } from "react";
import Error from "./Error";
import shortid from "shortid";

function Formulario({ agregarNuevoGasto }) {
  const [nombre, setNombre] = useState("");
  const [cantidad, setCantidad] = useState(0);
  const [error, setError] = useState(false);

  const handleClick = (e) => {
    e.preventDefault();

    if (nombre.trim() === "" || cantidad < 1 || isNaN(cantidad)) {
      setError(true);
      return;
    }

    setError(false);

    const gasto = {
      nombre,
      cantidad,
      id: shortid.generate(),
    };

    agregarNuevoGasto(gasto);
    setNombre("");
    setCantidad(0);
  };

  return (
    <form>
      <h1>Agrega tus gastos aqui</h1>

      {error ? <Error mensaje="Todos los campos son obligatorios" /> : null}

      <div className="campo">
        <label>Nombre Gasto</label>
        <input
          type="text"
          className="u-full-widht"
          placeholder="Ej. Cine"
          value={nombre}
          onInput={(e) => setNombre(e.target.value)}
        />

        <label>Cantidad Gasto</label>
        <input
          type="number"
          className="u-full-width"
          placeholder="Ej. 300"
          value={cantidad}
          onInput={(e) => setCantidad(parseInt(e.target.value))}
        />
      </div>

      <button
        type="submit"
        className="button-primary u-full-widht"
        onClick={handleClick}
      >
        Agregar Gasto
      </button>
    </form>
  );
}

export default Formulario;
