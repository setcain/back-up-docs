Ansible & Docker


-------------------------------------------------------------------------------

Arquitectura de archivos:
group_vars/
roles/
ansible.cfg
app.yml
munin.yml
site.yml
Vagrantfile

-------------------------------- vagrantfile ----------------------------------

vagrant.config(2) do |config|
    config.vm.box = "debian/jessie64"
    
    config.vm.define "munin" do |sut|
        config.vm.network "public_network"
    end

    config.vm.define "app" do |sut|
        config.vm.network "public_network"
    end

    config.vm.provision "ansible" do |ansible|
        ansible.playbook = "site.yml"
        ansible.sudo = true

        ansible.groups = {
            "munin" => ["munin"],
            "app" => ["app"]
        }
        end
end

------------------------------- site.yml --------------------------------------

---
- include: munin.yml
- include: app.yml

-------------------------------------------------------------------------------



