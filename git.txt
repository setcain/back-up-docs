------------------------------- MY ALIASES ----------------------------------
Alias:
git config --global alias.<alias name> 'git command, whitout git word'

Ejemplo:
git config --global alias.co 'checkout' (crear alias)
git config --global --unset alias.name (eliminar alias)
git config --global --get-regexp alias (para ver los alias creados)
git alias = git config --global --get-regexp alias (para ver los alias)

git logcomplete = git log --oneline --decorate --graph --all (muestra los log al completo)

---
git co = git checkout (permite cambiar de rama o de commit)

git ab <branch name> = git checkout -b <branch name> (crear una rama e ingresar en ella)

git mb <branch name> <new name> = git branch -m <branch name> <new name> (modificar nombre de la rama)

git db <branch name> = git branch -D <branch name> (eliminar rama)

git allb = git branch (ver las ramas existentes)

---
git rmchange <file> = git checkout -- <file> (borra últimas modificaciones hechas en un fichero )

git rmstaging <file> = git reset HEAD <file> (quita el fichero del staging y lo regresa al working)

git rmcommit <hash> = git reset <hash> (hash del commit al que quieres regresar, los cambios anteriores los deja en el working)

git rmall <hash> = git reset --hard <hash> (hash del commit al que quires regresar, elimina los anteriores cambios al completo)

---
git hide = git stash save "mensaje" (Esconder cambios en una rama sin necesidad de hacer commit)

git hidels = git stash list (Listar los cambios escondidos en una rama)
git hideap = git stash apply (Dejar de esconder los cambios)
git hided = git stash drop (Borrar la lista de los cambios escondidos)
git hidedd = git stash pop (apply y drop en un comando)


---------------- ESTRUCTURA DE GIT EN UN PROJECTO LOCAL ----------------------

El primer commit llamarlo initial commit sin hacer cambios en nada.
El segundo commit crear el proyecto con todo listo para desarrollar y llamarlo init project [name_project]

En cada commit se debe agregar:
Titulo corto del commit diciendo lo que hace el mismo
Contenido del commit separado por lista numerada de las acciones que hace

----- Ejemplo:

adds new app

1.- adds app_core for init the project
2.- adds home, about and contact static pages

-----

TODO:
1.- Si es necesario agregar un comentario TODO de lo que quedo pendiente en ese commit
para la proxima revicion iniciar con esa tarea.

2.- Se el commit termino pero hay un TODO en el proyecto anotarlo en otro commit, 
aparentado que ya se hizo, pero anotarlo abajo como pendiente con * y la ruta donde esta
el comentario TODO.

----- Ejemplo 1:

adds profile model

1.- adds profile model whit username and image field
2.- adds media config for deve env

TODO:
adds django signal


----- Ejemplo 2:

adds user update form

1.- add signals in app_user for register user whit default fields

TODO:
* add UserUpdateForm see -> app_user/forms.py


----------------------- CONFIGURACIONES PRINCIPALES --------------------------

git config --global user.name "nombre del usuario"   agregar usuario

git config --global user.name   		     ver usuario

git config --global user.email "correo del usuario"  agregar mail

git config --global color.ui true		     configuracion de colores

git config --global --list			     ver atributos en lista

------------------------ COMANDOS PRINCIPALES --------------------------------

Los 3 estado de Git:

Working directory - Inicio de proyecto, area de trabajo en rojo.

Staging area - Area de preparacion, listo para commit en verde.

Repository - Directorio y ficheros listos

----- paso 1, iniciar git
git init (iniciar working directory)

git status (estado de nuestro repository)

git add (agregar ficheros al staging area)
git add -A / git add . (agregar todos los ficheros de golpe)

----- paso 2, identificar cambios
git commit -m "escribir mensaje de commit directamente"
git commit -am "agrega los ficheros y puedes escribir el mensaje del commit"
git commit (abre el editor configurado para agregar el commit)
git commit --amend (editar el comentario del commmit o agregar mas modificaciones al commit)

//-----VER COMMITS
git log (lista los commits)
git log --oneline (muestra los commits solo con comentario principal)
git log --decorate (para ver los nombres de los punteros HEAD -> master)
git log --graph (ver el arbol grafico de ramas y commits)
git log --all (ver todos los commmits de todas las ramas)
git log > commits.txt (crea un .txt con el hash de los commits)

//-----VER DIFERENCIAS ENTRE FICHEROS
git diff (Ver que cambios se han hecho en un fichero)
git diff HEAD~1 HEAD (para ver la diferencia de un commit antes que head "el actual")
git diff HEAD~2 HEAD (para ver la diferencia de dos commits antes que head "el actual")
git diff HEAD~3 HEAD ...
git diff <hash> <hash> (ver diferencias entre un commit y otro)


------------------------------ ELIMINAR Y MODIFICAR --------------------------

SE PUEDE CAMBIAR EL <hash> POR LA PALABRA HEAD, HEAD~1, HEAD~2, ETC.
ejemplo:
git reset --hard HEAD~2 (regresar a como estaba el proyecto dos commits atras borrando todo)


# para borrar las últimas modificaciónes hechas en un fichero
git checkout -- <file> (borrar los cambios hechos en el working-directory)

# para borrar los ficheros agregados con add
git reset HEAD <file> (quitar del staging-area y regresarlo al working-dir)

# es útil para despues usar git reset head <file> o para hacer --amend y modificar el mensaje del commit
git reset --soft <hash> (hash del commit al que quires regresar, los últimos cambios los deja en el staging)
git reset --soft HEAD~3 (Quitar varios commits y juntarlos en uno solo)
git commit (Commit que une a los 3 commits fucionados)

# volver al estado inicial
git reset --hard <hash> (hash del commit al que quires regresar, elimina los últimmos cambios al completo)
git addcomplete = git add . && git commit (agrega todos los ficheros y abre el editor para escribir el commit)

git reset <hash> (hash del commit al que quieres regresar, los últimos cambios los deja en el working)


//----- GIT REVERT, FORMA SEGURA DE MODIFICAR

# es como hacer lo contrario de lo que ya esta hecho borrar lo que se agrego y agregar lo que se borro
git revert HEAD (volver al estado antes del commit, se guarda commit como revert)

# hacer revert de varios commits, revert ayuda con las modificaciones en remoto
git revert --no-commit HEAD~2
git revert --continue (guarda la reversión en un solo commit)

ejemplo:
git revert --no-commit HEAD
git revert --no-commit HEAD~1
git revert --no-commit HEAD~2
git revert --continue (revierte varios commits y al último se guarda en un solo revert)


-------------------------- RAMAS Y FUSIONES -------------------------------------

git branch -h (ver comandos de branch)

git branch (muestra las ramas existentes)

git branch <nombre de rama nueva> (crear una rama nueva) 

git checkout <branch name> (movernos a rama indicada, en esta rama se quedaran los cambios que hagamos) 

git checkout -b <branch name> (crear rama y entrar en ella en un solo comando)                                  

git branch -m <branch name> <new branch name> (editar nombre de la rama) 

git branch -d <branch name> (borrar rama creada, comprobando primero si esta fusionada a master)

git branch -D <branch name> (borrar rama creada a la fuerza)

# git no elimina las ramas ni commits, sinó que oculta los elementos de la vista
git gc (Recolector de basura, compacta al maximo los elementos ocultos, para reducir espacio)

git checkout master (regresar a la rama master, si vemos el sitio web estara como lo dejamos antes)

Fusionar ramas: 
# entrar a MASTER y desde ahi
git merge <branch name> (Master absorbe a <branch name> con sus cambios)
# cuando se hace otra rama y se fusiona con la rama master se soluciona con fusion recursiva y se agrega un commit automático

Fusionar ramas con conflicto:
# los conflictos se dan cuando se modifica la misma línea del mismo fichero
<<<<<<< HEAD (commit actual)
    modificación 1
=======
    modificación 2
>>>>>>> brach-name (rama donde esta el segundo commit)
# primero elegir la modificación que queramos que permanezca, despues eliminar la otra y al último los comentarios (<<>>==)
# git add y git commit
# deshecer la solución despues de add y commit
git merge --abort

Se puede sustituir merge por rebase para eliminar los grafos y tener mas limpio el historial de log


--------------------------------- TAGS ---------------------------------------

Los tags usualmente se usan con el versionado semántico del proyecto, por ejemplo V1.0.0

Etiquetas:
Estan en .git/ref/tags

Agregar tags ligeros:
git tag <tag name> crear un tag que se agrega en el HEAD o último commit agregado al repositorio
git tag <tag name> <hash> crer un tag en commit anterior

Agregar tags anotados:
git tag -a <tag name> abre el editor como si se tratara de un commit para anotarlo con título y contenido
git show <anotated tag name> ver los tags anotados, el show sirve para ver info de commits tambien

Info de tags:
git tag -l ver los tags
git tag -l "v0.0.*" filtrar tags, filtra todols los tags que inicien don v0.0.
git tag -d <tag name> borra tag

git checkout <tag name> viajar al tag de una versión especifica de el código


------------------------- REPOSITORIOS REMOTOS -------------------------------

Los remotos son un monton de ramas mas en nuestro proyecto.
git branch --all (despues de descargar de un remoto para ver las ramas que se crean)

git remote (ver los remotos que tenemos)

git remote add origin "url de algun hub" (ejemplo github)

git remote -v (comprobar que estan conectados por url)

git push origin master (o la ramificacion que queramos, para subir los branch al remoto)

git fetch origin (preguntar al remoto si hay actualizaciónes y descargarlas)

git pull origin master (hace lo mismo que fetch y ademas fuciona (merge) el remoto con el local)

git pull --rebase origin master (evitar los commits intermedios como en pull)

git clone "url a clonar" (clonar proyecto en mi maquina local)

git remote remove origin (desconectar el proyecto)


------------------- AGREGAR LLAVES SSH EN GITHUB -----------------------------

En el directorio .ssh se guardan las llaves publicas y privadas.

Crea las llaves, es aconsejable cambiar el nombre de las llaves por otro
ssh-keygen -t rsa -b 1024

Agregar la llave privada al Agente ssh para hacerlas validas
eval &(ssh-agent -s)
ssh-add ~/.ssh/nombre_llavePrivada

Agregar un repositorio a tu maquina con ssh no con http
git remote add origin git@github.com:xxx/xxx.git

Cada vez hagas un git push a github no te pedira contraseña.

Si pierdes la llave, debes de entrar a github borrarla y crear otra nueva


----------------------------- GITHUB PAGES -----------------------------------

Nombar el repositorio como username.github.io
Crear un archivo llamando CNAME
Dentro de este archivo poner tu nombre de dominio setcain.me


------------------------ INVESTIGAR COMO USARLOS -----------------------------


git stash (Esconder directorio de trabajo y mostrarlo como limpio)
git stash list (Ver lo que hay en el stash, con un id, rama y último commit)
git stash drop (Eliminar el stash)

Unir varios commits en uno solo con rebase, en este caso los últimos 4
git rebase -i HEAD~4 (Esto abre un editor de texto para elegir entre las múltiples opciones)
